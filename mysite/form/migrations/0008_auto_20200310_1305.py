# Generated by Django 3.0.4 on 2020-03-10 12:05

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('form', '0007_auto_20200310_1256'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='time_new',
            field=models.TimeField(default=datetime.datetime(2020, 3, 10, 13, 29, 59, 801157)),
        ),
    ]
