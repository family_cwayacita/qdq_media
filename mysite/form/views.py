from django.shortcuts import render
from datetime import datetime, timedelta
from .forms import task_form
from .tables import MyTable
from .models import Task

def input_tasks(request):
    form = task_form(request.POST)
    # check whether it's valid:
    if form.is_valid():
        #input the teoric moment to work on the task
        p = Task(task_text=form['task_name'].value(), task_date=datetime.now(),time_new= (datetime.now() + timedelta(minutes=25) ))
        p.save()
    #prepare the table to be displayed
    table = MyTable(Task.objects.all())
    #rendering the table and form into the html
    #requesting the filter object
    pks = request.POST.getlist("selection")
    #filtering on the selected task by the user
    selected_objects = Task.objects.filter(pk__in=pks)
    #updating the new time to work ion the task
    for selected_object in selected_objects:
        selected_object.archive_post
    # rendering the filtered table and message
    return render(request, 'form/form_output.html', {'table': table, 'table2' : selected_objects ,'form': form, 'room_name': 'channel_task'})

