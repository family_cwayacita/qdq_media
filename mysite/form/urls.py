from django.urls import path
from form.views import input_tasks

#one url in this projectcontaining table, form and filtered task accessible at http://127.0.0.1:8000/form/form_output/
urlpatterns = [
    path('form_output/', input_tasks),
]