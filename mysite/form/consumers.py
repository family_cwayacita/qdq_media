import asyncio

from channels.generic.websocket import AsyncWebsocketConsumer
import json
from datetime import datetime

class ChatConsumer(AsyncWebsocketConsumer):
    async def connect(self):
        self.room_group_name = 'task_layer'

        # Join room group
        # Join room group
        await self.channel_layer.group_add(
            self.room_group_name,
            self.channel_name
        )

        await self.accept()

        await self.isalive()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            self.room_group_name,
            self.channel_name
        )

    # Receive message from WebSocket
    async def receive(self, text_data):
        text_data_json = json.loads(text_data)
        message = text_data_json['message']

        # Send message to room group
        await self.channel_layer.group_send(
            self.room_group_name,
            {
                'type': 'chat_message',
                'message': message
            }
        )

    # Receive message from room group
    async def chat_message(self, event):
        message = event['message']
        def myconverter(o):
            if isinstance(o, datetime):
                return o.__str__()

            # Send message to WebSocket
        await self.send(text_data=json.dumps({
            'message': message
        }, default=myconverter))


    async def isalive(self):
        time= datetime.now()
        await self.chat_message({'message': 'your task begins'})
        await self.chat_message({'message': time})
        await asyncio.sleep(20)
        await self.chat_message({'message': 'your task is finished'})
        await self.chat_message({'message': time})

